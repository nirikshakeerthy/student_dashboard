# Student_Dashboard

Student data is loaded to Elasticsearch using python and the data is visualized using Kibana

200 students data was uploaded to elasticsearch using Python and elasticsearch API.

## Steps followed:

1. Installed Elasticsearch and Kibana and elasticsearch API
2. Created the index using: 

``` json
Index name: students_info_temp
PUT /students_info_temp
{
"mappings": {
"properties": {
"roll_no": {
"type": "integer"
      },
"name": {
"type": "text"
  },
"physics_marks": {
"type": "integer",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
  },
"chemistry_marks": {
"type": "integer",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
  },
"maths_marks": {
"type": "integer",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
  },
"result": {
"type": "text"
  }
    }
  }
}
```
3. Uploaded the data using elastic_load.python
4. Created the Pie chart using bucket aggregation based on terms(result) for pass vs fail pie chart
5. created scripted feilds for total score, percentage and student_class
6. created pie chart to show split of classes using scipted fields
no class < 35%
35% >= second class < 60 %
60% >= first class < 85 %
85% >= distinction
7. created the list of top scorers in each subject using kibana lens.

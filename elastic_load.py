from elasticsearch import Elasticsearch, helpers
import csv

es = Elasticsearch(host = "localhost", port = 9200)

with open('/home/Downloads/st.csv') as f:
    reader = csv.DictReader(f)
    helpers.bulk(es, reader, index='students_info_temp')

